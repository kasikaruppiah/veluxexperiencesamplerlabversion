package edu.northeastern.ledlab.veluxexperiencesamplerlab

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import kotlinx.android.synthetic.main.activity_dot_memory_configuration.*

class DotMemoryConfiguration : BaseActivity() {
    private var assessmentVal = 20
    //    private var assessmentVal = 1
    var setsizeVal = 3
    var practicetrialsVal = 3

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dot_memory_configuration)
        updateBegin()

        setsize.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                //TODO("not implemented") //To change body of created functions use File |
                // Settings | File Templates.
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                //TODO("not implemented") //To change body of created functions use File |
                // Settings | File Templates.
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                setsizeVal = if (p0.toString().isNotEmpty())
                    p0.toString().toInt()
                else
                    0
                updateBegin()
            }
        })

        practicetrials.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                //TODO("not implemented") //To change body of created functions use File |
                // Settings | File Templates.
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                //TODO("not implemented") //To change body of created functions use File |
                // Settings | File Templates.
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                practicetrialsVal = if (p0.toString().isNotEmpty())
                    p0.toString().toInt()
                else
                    0
            }
        })

        begin.setOnClickListener {
            begin.isEnabled = false
            val intent = Intent(this, DotMemoryTask::class.java)
            intent.putExtra("assessment", assessmentVal)
            intent.putExtra("setsize", setsizeVal)
            intent.putExtra("practicetrials", practicetrialsVal)
            startActivity(intent)
            finish()
        }
    }

    private fun updateBegin() {
        begin.isEnabled = (setsizeVal > 0)
    }
}
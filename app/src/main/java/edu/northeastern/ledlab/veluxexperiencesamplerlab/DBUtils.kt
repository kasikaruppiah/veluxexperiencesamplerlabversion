package edu.northeastern.ledlab.veluxexperiencesamplerlab

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.preference.PreferenceManager
import android.util.Log
import android.widget.Toast
import com.evernote.android.job.JobManager
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import com.google.gson.Gson
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


/**
 * Created by ledlab on 8/20/17.
 */
object DBUtils {

    private fun updateUI(context: Context, msg: String) {
        val activity = context as Activity
        activity.runOnUiThread({
            Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
            activity.finish()
        })
    }

    private fun getDatabase(): FirebaseFirestore {

        return FirebaseFirestore.getInstance()
    }

    private fun loadMainActivity(activity: Activity, msg: String) {
        val intent = Intent(activity, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        activity.startActivity(intent)
        updateUI(activity, msg)
    }

    fun registerUser(activity: Activity) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(activity)

        val participantId = sharedPref.getString(activity.getString(R.string.pid), "unknown")
        val zurichParticipant = participantId.startsWith("z", true)

        val prefEditor = sharedPref.edit()
        prefEditor.putBoolean(activity.getString(R.string.preg), true)
        prefEditor.putInt("Burst", 0)
        prefEditor.putInt("TotalSurveys", 0)
        prefEditor.putInt("TotalProofs", 0)
        prefEditor.putBoolean("SalivaSampling", zurichParticipant)
        prefEditor.putBoolean("HairSampling", zurichParticipant)
        prefEditor.putString("Goal1", "Stay healthy")
        prefEditor.putString("Goal2", "Successful career")
        prefEditor.putString("Goal3", "Be assertive")
        prefEditor.putInt("MeanSetSize", 3)
        prefEditor.putBoolean("ScheduleSurvey", true)
        prefEditor.apply()

        loadMainActivity(activity, activity.getString(R.string.reg_success))
    }

    fun addConfigurationScore(activity: Activity, userInfo: Map<String, Any>) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(activity)
        val pid = sharedPref.getString(activity.getString(R.string.pid), "unknown")
        val db = DBUtils.getDatabase()
        val prefEditor = sharedPref.edit()
        prefEditor.putInt("MeanSetSize", ((userInfo["Calibration"] as Map<String, Any>)["MeanSetSize"] as Double).toInt())
        prefEditor.apply()
        db.collection("Users")
                .document(pid)
                .set(userInfo, SetOptions.merge())
    }

    fun retrieveUserGoals(context: Context) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val localGoal1 = sharedPref.getString("Goal1", "")
        val localGoal2 = sharedPref.getString("Goal2", "")
        val localGoal3 = sharedPref.getString("Goal3", "")
        if (localGoal1.isNullOrEmpty() || localGoal2.isNullOrEmpty() || localGoal3.isNullOrEmpty()) {
            val participantId = sharedPref.getString(context.getString(R.string.pid), "unknown")
            val burst = sharedPref.getInt("Burst", 0)
            val db = DBUtils.getDatabase()
            val docRef = db.collection("Users").document(participantId)
            docRef.get().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val document = task.result
                    if (document != null && document.exists()) {
                        val goals = if (burst == 0) (document.data["Goals"] as Map<String, String>) else (document.data["Burst$burst"] as Map<String, Any>)["Goals"] as Map<String, String>
                        val goal1 = goals["Goal1"]
                        val goal2 = goals["Goal2"]
                        val goal3 = goals["Goal3"]
                        if (goal1 == null || goal2 == null || goal3 == null) {
                            updateUI(context, context.getString(R.string.goal_not_updt))
                        } else {
                            val prefEditor = sharedPref.edit()
                            prefEditor.putString("Goal1", goal1)
                            prefEditor.putString("Goal2", goal2)
                            prefEditor.putString("Goal3", goal3)
                            prefEditor.apply()

                            goalsRetrieved(context)
                        }
                    } else {
                        Log.w("retrieveUserGoals", "No such document")
                        updateUI(context, context.getString(R.string.data_not_found))
                    }
                } else {
                    Log.w("retrieveUserGoals::F", "get failed with ", task.exception)
                    updateUI(context, context.getString(R.string.user_data_failed))
                }
            }
        } else
            goalsRetrieved(context)
    }

    private fun goalsRetrieved(context: Context) {
        updateUI(context, context.getString(R.string.goal_retrieved))
        val intent = Intent(context, LoadingActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        intent.putExtra("addNotification", true)
        context.startActivity(intent)
    }

    fun addBurst(context: Context, timeMap: TimeMap) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val pid = sharedPref.getString(context.getString(R.string.pid), "unknown")
        val burst = sharedPref.getInt("Burst", 0)
        val totalSurveys = sharedPref.getInt("TotalSurveys", 0)
        val totalProofs = sharedPref.getInt("TotalProofs", 0)

        val prefEditor = sharedPref.edit()
        val json = Gson().toJson(timeMap)
        prefEditor.putInt("StartSurvey", totalSurveys + 1)
        prefEditor.putInt("FirstProof", totalProofs + 1)
        prefEditor.putString(context.getString(R.string.time_map_key), json)
        prefEditor.putInt("PenalityDays", 0)
        prefEditor.putBoolean("ScheduleSurvey", true)
        prefEditor.putLong("LastDay", timeMap.endDate.time - 86400000)
        prefEditor.apply()

        val db = DBUtils.getDatabase()

        val goals = HashMap<String, String>()
        goals["Goal1"] = sharedPref.getString("Goal1", "")
        goals["Goal2"] = sharedPref.getString("Goal2", "")
        goals["Goal3"] = sharedPref.getString("Goal3", "")

        val burstData = HashMap<String, Any>()
        burstData["StartDate"] = timeMap.startDate
        burstData["EndDate"] = timeMap.endDate
        burstData["PenalityDays"] = 0
        burstData["StartSurvey"] = totalSurveys + 1
        burstData["EndSurvey"] = totalSurveys
        burstData["TotalSurveys"] = 0
        burstData["Goals"] = goals
        burstData["TotalProofs"] = 0
        burstData["FirstProof"] = totalProofs + 1
        burstData["LastProof"] = totalProofs
        val userInfo = HashMap<String, Any>()
        userInfo["Burst${burst + 1}"] = burstData

        db.collection("Users")
                .document(pid)
                .set(userInfo, SetOptions.merge())

        loadMainActivity(context as Activity, context.getString(R.string.scheduled_notifications))
    }

    fun addSurvey(activity: Activity, pid: String, burst: String, surveyId: Int, date: String, surveyTime: Int, newUserResponse: HashMap<String, Any>) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(activity)

        val prefEditor = sharedPref!!.edit()
        prefEditor.putInt("TotalSurveys", surveyId)
        val timeMapObj = Gson().fromJson(sharedPref.getString(activity.getString(R.string.time_map_key), "'"), TimeMap::class.java)
        val dateMap = timeMapObj.timeMap[date]
        dateMap!!.completedSurveys.add(surveyTime)
        val json = Gson().toJson(timeMapObj)
        prefEditor.putString(activity.getString(R.string.time_map_key), json)
        prefEditor.apply()

        val db = DBUtils.getDatabase()

        val userInfo = HashMap<String, Any>()
        userInfo["TotalSurveys"] = surveyId
        val burstData = HashMap<String, Any>()
        burstData["EndSurvey"] = surveyId
        burstData["TotalSurveys"] = surveyId - sharedPref.getInt("StartSurvey", 0) + 1
        userInfo[burst] = burstData

        db.collection("Users").document(pid).set(userInfo, SetOptions.merge())
        db.collection("Users")
                .document(pid)
                .collection("Surveys")
                .document("$surveyId")
                .set(newUserResponse)
    }

    fun updateBurstCompletion(context: Context) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val pid = sharedPref.getString(context.getString(R.string.pid), "unknown")
        val burst = sharedPref.getInt("Burst", 0) + 1

        val prefEditor = sharedPref!!.edit()
        prefEditor.putInt("Burst", burst)
        prefEditor.putBoolean("ScheduleSurvey", false)
        prefEditor.apply()

        val db = DBUtils.getDatabase()

        val userInfo = HashMap<String, Any>()
        userInfo["Burst"] = burst

        db.collection("Users")
                .document(pid)
                .set(userInfo, SetOptions.merge())
    }

    fun schedulePenalityJob(context: Context, timeMS: Long) {
        val targetTime = Calendar.getInstance()
        targetTime.timeInMillis = timeMS
        val simpleDateFormat = SimpleDateFormat("dd/M/yyyy")
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val pid = sharedPref.getString(context.getString(R.string.pid), "unknown")
        val burst = "Burst${sharedPref.getInt("Burst", 0) + 1}"
        val timeMapObj = Gson().fromJson(sharedPref.getString(context.getString(R.string.time_map_key), ""), TimeMap::class.java)
        val dayMap = timeMapObj.timeMap[simpleDateFormat.format(targetTime.time)]
        if (dayMap!!.completedSurveys.size < 5) {
            val penalityDays = sharedPref.getInt("PenalityDays", 0)
            if (penalityDays < 3)
                addPenalityDay(context, pid, burst)
        }
    }

    private fun addPenalityDay(context: Context, pid: String, burst: String) {
        JobManager.instance().cancelAllForTag(LastDayJob.TAG)

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val timeMapObj = Gson().fromJson(sharedPref.getString(context.getString(R.string.time_map_key), ""), TimeMap::class.java)
        val targetDate = Calendar.getInstance()
        targetDate.time = timeMapObj.endDate

        val weekdayWake = sharedPref.getInt(context.getString(R.string.weekday_wake_key), 360)
        val weekdaySleep = weekdayWake + 720
        val weekendWake = sharedPref.getInt(context.getString(R.string.weekend_wake_key), 480)
        val weekendSleep = weekendWake + 720
        val penalityDays = sharedPref.getInt("PenalityDays", 0) + 1

        val simpleDateFormat = SimpleDateFormat("dd/M/yyyy")
        val dayMap = NotifyJob.scheduleNotificationForDay(context, targetDate, weekdayWake, weekdaySleep, weekendWake, weekendSleep)

        timeMapObj.timeMap[simpleDateFormat.format(targetDate.time)] = dayMap
        targetDate.add(Calendar.DATE, 1)
        val burstTimeMap = TimeMap(timeMapObj.timeMap, timeMapObj.startDate, targetDate.time)
        LastDayJob.scheduleJob(targetDate.timeInMillis)

        val prefEditor = sharedPref.edit()
        val json = Gson().toJson(burstTimeMap)
        prefEditor.putString(context.getString(R.string.time_map_key), json)
        prefEditor.putInt("PenalityDays", penalityDays)
        prefEditor.apply()

        val db = DBUtils.getDatabase()

        val userInfo = HashMap<String, Any>()
        val burstData = HashMap<String, Any>()
        burstData["PenalityDays"] = penalityDays
        burstData["EndDate"] = targetDate.time
        userInfo[burst] = burstData

        db.collection("Users")
                .document(pid)
                .set(userInfo, SetOptions.merge())
    }

    fun addProof(activity: Activity, pid: String, date: String, wakeUpTime: Date, samplingTime: Date, scheduleNotification: Boolean): Int {
        Log.d("KAKA::addProof", "inside")
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(activity)
        val photoId = sharedPref.getInt("TotalProofs", 0) + 1
        val burst = sharedPref.getInt("Burst", 0) + 1

        val prefEditor = sharedPref!!.edit()
        val timeMapObj = Gson().fromJson(sharedPref.getString(activity.getString(R.string.time_map_key), "'"), TimeMap::class.java)
        val dateMap = timeMapObj.timeMap[date]
        val newDateMap = DayMap(dateMap!!.surveyTimes, dateMap.completedSurveys, wakeUpTime)
        timeMapObj.timeMap[date] = newDateMap
        val json = Gson().toJson(timeMapObj)
        prefEditor.putString(activity.getString(R.string.time_map_key), json)
        prefEditor.putInt("TotalProofs", photoId)
        prefEditor.apply()
        if (scheduleNotification)
            NotifyJob.scheduleSamplingNotifications(activity, wakeUpTime)

        val db = DBUtils.getDatabase()

        val userInfo = HashMap<String, Any>()
        userInfo["TotalProofs"] = photoId
        val burstData = HashMap<String, Any>()
        burstData["LastProof"] = photoId
        burstData["TotalProofs"] = photoId - sharedPref.getInt("FirstProof", 0) + 1
        userInfo["Burst$burst"] = burstData

        db.collection("Users").document(pid).set(userInfo, SetOptions.merge())

        val userProof = HashMap<String, Any>()
        userProof["WakeTime"] = wakeUpTime
        userProof["SamplingTime"] = samplingTime

        db.collection("Users")
                .document(pid)
                .collection("Proofs")
                .document("$photoId")
                .set(userProof)

        return photoId
    }

    fun updateURI(pid: String, proofId: Int, proofURI: String) {
        Log.d("KAKA::updateURI", "inside Function")
        val db = DBUtils.getDatabase()
        val userProof = HashMap<String, Any>()
        userProof["URI"] = proofURI
        db.collection("Users")
                .document(pid)
                .collection("Proofs")
                .document("$proofId")
                .set(userProof, SetOptions.merge())
    }

    fun syncData(context: Context) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val participantId = sharedPref.getString(context.getString(R.string.pid), "unknown")

        val db = DBUtils.getDatabase()

        db.collection("Users").document(participantId).get()
    }
}
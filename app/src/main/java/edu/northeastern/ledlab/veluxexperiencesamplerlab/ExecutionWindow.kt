package edu.northeastern.ledlab.veluxexperiencesamplerlab

import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by ledlab on 9/3/17.
 */
class ExecutionWindow(targetDay: Int, targetHour: Int, targetMinute: Int) {
    var timeMs = 0L

    init {
        val calendar = Calendar.getInstance()
        val currentDay = calendar.get(Calendar.DAY_OF_WEEK)
        val currentHour = calendar.get(Calendar.HOUR_OF_DAY)
        val currentMinute = calendar.get(Calendar.MINUTE)

        var dayOffset = TimeUnit.DAYS.toMillis((targetDay - currentDay).toLong())
        val hourOffset = TimeUnit.HOURS.toMillis((targetHour - currentHour).toLong())
        val minuteOffset = TimeUnit.MINUTES.toMillis((targetMinute - currentMinute).toLong())

        if (targetDay <= currentDay)
            dayOffset += TimeUnit.DAYS.toMillis(7)

        timeMs = dayOffset + hourOffset + minuteOffset
        if (timeMs < 0)
            timeMs = 1
    }
}
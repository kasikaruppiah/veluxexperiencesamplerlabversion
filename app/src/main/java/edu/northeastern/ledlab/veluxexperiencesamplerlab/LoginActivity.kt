package edu.northeastern.ledlab.veluxexperiencesamplerlab

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        participant_id.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                //TODO("not implemented") //To change body of created functions use File |
                // Settings | File Templates.
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                //TODO("not implemented") //To change body of created functions use File |
                // Settings | File Templates.
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                register_button.isEnabled = p0.toString().trim().isNotEmpty()
            }
        })

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        val pId = sharedPref.getString(getString(R.string.pid), "")
        participant_id.setText(pId)

        register_button.setOnClickListener { view ->
            kotlin.run {
                val pid = participant_id.text.toString().trim()
                val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
                val prefEditor = sharedPref.edit()
                prefEditor.putString(getString(R.string.pid), pid)
                prefEditor.apply()
                val language = if (pid.startsWith("z", true)) "de" else "en"
                LocaleManager.setLocale(this, language)
                val intent = Intent(this, LoadingActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                intent.putExtra("registerUser", true)
                startActivity(intent)
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState!!.putString("pid", participant_id.text.toString().trim())
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        val pid = savedInstanceState!!.getString("pid")
        participant_id.text = SpannableStringBuilder(pid)
        register_button.isEnabled = pid.isNotEmpty()
    }
}

package edu.northeastern.ledlab.veluxexperiencesamplerlab

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_survey.*
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by ledlab on 8/15/17.
 */
class SurveyFragment : android.support.v4.app.Fragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val surveyMenu = inflater!!.inflate(R.layout.fragment_survey, container, false)
        val button = surveyMenu.findViewById<Button>(R.id.survey_button)
        val imageButton = surveyMenu.findViewById<Button>(R.id.image_button)
        refreshTab(button, imageButton)
        return surveyMenu
    }

    override fun onResume() {
        super.onResume()
        refreshTab(survey_button, image_button)
    }

    private fun refreshTab(button: Button, imageButton: Button) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(activity)
        val sampling = sharedPref.getBoolean("SalivaSampling", false)
        val meanSetSize = sharedPref.getInt("MeanSetSize", -1)
        val burst = sharedPref.getInt("Burst", 0) + 1
        val scheduled = sharedPref.getBoolean("ScheduleSurvey", false)
        val timeMap = Gson().fromJson(sharedPref.getString(context.getString(R.string.time_map_key), ""), TimeMap::class.java)
        imageButton.visibility = View.GONE

        if (meanSetSize == -1) {
            button.text = getString(R.string.dot_memory_task)
            button.setOnClickListener({ view ->
                kotlin.run {
                    val intent = Intent(context, DotMemoryConfiguration::class.java)
                    startActivity(intent)
                }
            })
        } else {
            if (scheduled) {
                button.text = getString(R.string.sur)
                button.setOnClickListener({ view ->
                    kotlin.run {
                        val surveyCheck = timeCheck(timeMap)
                        if (surveyCheck.check) {
                            val intent = Intent(context, QuestionnaireActivity::class.java)
                            intent.putExtra("date", surveyCheck.date)
                            intent.putExtra("time", surveyCheck.time)
                            intent.putExtra("dailyCount", surveyCheck.dailyCount)
                            startActivity(intent)
                        } else {
                            Toast.makeText(context, getString(R.string.try_later), Toast.LENGTH_SHORT).show()
                        }
                    }
                })
                if (sampling) {
                    imageButton.visibility = View.VISIBLE
                    imageButton.text = getString(R.string.upld_img)
                    imageButton.setOnClickListener { view ->
                        kotlin.run {
                            val intent = Intent(context, HormoneSamplingActivity::class.java)
                            val time = sharedPref.getInt(context.getString(R.string.weekday_wake_key), 360)
                            val calendar = Calendar.getInstance()
                            calendar.set(Calendar.HOUR_OF_DAY, time / 60)
                            calendar.set(Calendar.MINUTE, time % 60)
                            intent.putExtra("WakeUpTime", calendar.time)
                            startActivity(intent)
                        }
                    }
                }
            } else {
                button.text = getString(R.string.schedule)
                if (burst < 6) {
                    button.setOnClickListener({ view ->
                        kotlin.run {
                            val alertDialog = AlertDialog.Builder(context)
                            alertDialog.setCancelable(false)
                            alertDialog.setMessage(String.format(context.getString(R.string.schedule_survey), burst))
                            alertDialog.setPositiveButton(getString(R.string.ok)) { p0, p1 ->
                                kotlin.run {
                                    var intent = Intent(context, GoalActivity::class.java)
                                    if (burst == 1) {
                                        intent = Intent(context, LoadingActivity::class.java)
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                        intent.putExtra("scheduleSurvey", true)
                                    }
                                    startActivity(intent)
                                }
                            }
                            alertDialog.setNegativeButton(getString(R.string.cancel)) { p0, p1 -> }
                            alertDialog.show()
                        }
                    })
                } else {
                    button.isEnabled = false
                }
            }
        }
    }

    private fun timeCheck(timeMapObj: TimeMap?): SurveyCheck {
        val calendar = Calendar.getInstance()

        val simpleDateFormat = SimpleDateFormat("dd/M/yyyy")
        val date = simpleDateFormat.format(Date())
        val currTime = calendar.get(Calendar.HOUR_OF_DAY) * 60 + calendar.get(Calendar.MINUTE)

        return SurveyCheck(true, date, currTime, 0)
    }

    private fun samplingCheck(burst: Int, sampling: Boolean, timeMapObj: TimeMap): Boolean {
        var samplingBoolean = arrayListOf(1, 3, 5).contains(burst) and sampling

        if (samplingBoolean) {
            val currentDate = Calendar.getInstance().time
            val startDate = Calendar.getInstance()
            startDate.time = timeMapObj.startDate
            startDate.add(Calendar.DATE, 1)
            val samplingStartDate = startDate.time
            startDate.add(Calendar.DATE, 2)
            val samplingEndDate = startDate.time

            samplingBoolean = ((currentDate.after(samplingStartDate) or (currentDate == samplingStartDate)) and currentDate.before(samplingEndDate))
        }

        return samplingBoolean
    }
}
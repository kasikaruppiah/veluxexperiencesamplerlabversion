package edu.northeastern.ledlab.veluxexperiencesamplerlab

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import android.os.Build
import com.evernote.android.job.JobManager


/**
 * Created by ledlab on 9/1/17.
 */
class App : Application() {
    override fun onCreate() {
        super.onCreate()
        JobManager.create(this).addJobCreator(JARVIS())
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
            JobManager.instance().config.isAllowSmallerIntervalsForMarshmallow = true
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        LocaleManager.setLocale(this)
    }
}

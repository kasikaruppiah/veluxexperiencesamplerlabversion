package edu.northeastern.ledlab.veluxexperiencesamplerlab

import com.evernote.android.job.Job
import com.evernote.android.job.JobCreator


/**
 * Created by ledlab on 9/1/17.
 */
class JARVIS : JobCreator {
    override fun create(tag: String?): Job? = when (tag) {
        SyncJob.TAG -> SyncJob()
        NotifyJob.TAG -> NotifyJob()
        LastDayJob.TAG -> LastDayJob()
        PenalityJob.TAG -> PenalityJob()
        else -> null
    }
}
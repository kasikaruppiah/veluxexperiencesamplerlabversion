package edu.northeastern.ledlab.veluxexperiencesamplerlab

import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import android.preference.PreferenceManager
import java.util.*

class LocaleManager {
    companion object {
        private val LANGUAGES = arrayListOf("en", "de")
        fun setLocale(context: Context): Context {
            return setLocale(context, getLanguage(context))
        }

        fun setLocale(context: Context, language: String): Context {
            persistLanguage(context, language)
            return updateResource(context, language)
        }

        private fun getLanguage(context: Context): String {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            val deviceLanguage = Resources.getSystem().configuration.locale.language
            val defaultLanguage = if (deviceLanguage in LANGUAGES) deviceLanguage else context.resources.getString(R.string.default_lang)
            return preferences.getString(context.resources.getString(R.string.language_key), defaultLanguage)
        }

        private fun persistLanguage(context: Context, language: String) {
            PreferenceManager.getDefaultSharedPreferences(context).edit().putString(context.resources.getString(R.string.language_key), language).apply()
        }

        private fun updateResource(context: Context, language: String): Context {
            val locale = Locale(language)
            Locale.setDefault(locale)

            val resource = context.resources
            val configuration = Configuration(resource.configuration)
            if (Build.VERSION.SDK_INT >= 17) {
                configuration.setLocale(locale)
                return context.createConfigurationContext(configuration)
            } else {
                configuration.locale = locale
                resource.updateConfiguration(configuration, resource.displayMetrics)
            }
            return context
        }
    }
}
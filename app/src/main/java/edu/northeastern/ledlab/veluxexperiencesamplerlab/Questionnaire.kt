package edu.northeastern.ledlab.veluxexperiencesamplerlab

/**
 * Created by ledlab on 8/19/17.
 */
data class Questionnaire(
        val questions: Array<Question>
)

data class Question(
        val type: String,
        val tag: String,
        val question: String,
        val options: Array<Option>,
        val optionfields: Array<OptionField>,
        val precondition: HashMap<String, Any>,
        val conditions: Array<Condition>,
        val nextquestion: Int,
        val anchortext: HashMap<String, String>
)

data class Option(
        val option: String,
        val image: String,
        val value: String
)

data class OptionField(
        val tag: String,
        val label: String,
        val options: Array<Option>
)

data class Condition(
        val option: String,
        val value: Int
)
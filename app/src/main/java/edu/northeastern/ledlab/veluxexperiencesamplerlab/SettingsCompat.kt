package edu.northeastern.ledlab.veluxexperiencesamplerlab

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.preference.Preference
import android.support.v7.preference.PreferenceFragmentCompat
import android.support.v7.preference.PreferenceManager


/**
 * Created by ledlab on 8/16/17.
 */
class SettingsCompat : PreferenceFragmentCompat(), Preference.OnPreferenceChangeListener {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences)

        val weekday_wake = findPreference(getString(R.string.weekday_wake_key))
        bindPreferenceSummaryToValue(weekday_wake)
        val weekend_wake = findPreference(getString(R.string.weekend_wake_key))
        bindPreferenceSummaryToValue(weekend_wake)
    }

    override fun onDisplayPreferenceDialog(dialogPreference: Preference) {

        var dialogFragment: DialogFragment? = null
        if (dialogPreference is TimePreference) {
            dialogFragment = TimePreferenceDialogFragmentCompat.newInstance(dialogPreference.getKey())
        }

        if (dialogFragment != null) {
            dialogFragment.setTargetFragment(this, 0)
            dialogFragment.show(this.fragmentManager, "android.support.v7.dialogPreference.PreferenceFragment.DIALOG")
        } else {
            super.onDisplayPreferenceDialog(dialogPreference)
        }

    }

    private fun bindPreferenceSummaryToValue(pref: Preference) {
        pref.onPreferenceChangeListener = this
        val defaultPref = PreferenceManager.getDefaultSharedPreferences(pref.context)
        val preferenceValue = if (pref is TimePreference) defaultPref.getInt(pref.key, 0) else defaultPref.getString(pref.key, "")
        onPreferenceChange(pref, preferenceValue)
    }

    override fun onPreferenceChange(pref: Preference?, newValue: Any?): Boolean {
        if (pref is TimePreference) {
            val minutesAfterMidnight = newValue as Int
            val hours = minutesAfterMidnight / 60
            val minutes = minutesAfterMidnight % 60
            val stringValue = "${String.format("%02d", hours)}:${String.format("%02d", minutes)}"
            pref.summary = stringValue
            val oldTime = pref.time
            if (oldTime != newValue) pref.time = newValue
        }
        return true
    }
}
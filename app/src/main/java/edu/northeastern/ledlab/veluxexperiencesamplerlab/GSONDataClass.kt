package edu.northeastern.ledlab.veluxexperiencesamplerlab

import java.util.*
import kotlin.collections.HashSet

data class SurveyResponse(
        val surveyid: Long,
        var createddate: String,
        var responses: ArrayList<HashMap<String, String>>
)

data class TimeMap(val timeMap: HashMap<String, DayMap>, val startDate: Date, val endDate: Date)

data class DayMap(val surveyTimes: ArrayList<Int>, val completedSurveys: HashSet<Int> = HashSet(), val wakeUpTime: Date? = null)

data class SurveyCheck(val check: Boolean, val date: String?, val time: Int?, val dailyCount: Int = 0)


package edu.northeastern.ledlab.veluxexperiencesamplerlab

import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import kotlinx.android.synthetic.main.fragment_upload.*
import java.util.*


/**
 * Created by ledlab on 8/15/17.
 */
class UploadFragment : android.support.v4.app.Fragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val uploadMenu = inflater!!.inflate(R.layout.fragment_upload, container, false)

        uploadMenu.findViewById<Button>(R.id.sync_data).setOnClickListener({ view ->
            kotlin.run {
                //                DBUtils.syncData(context)
            }
        })

        return uploadMenu
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            updateStats()
        }
    }

    private fun updateStats() {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val participantId = sharedPref.getString(context.getString(R.string.pid), "unknown")
        val totalSurveys = sharedPref.getInt("TotalSurveys", 0)
        val totalProofs = sharedPref.getInt("TotalProofs", 0)
        val burst = sharedPref.getInt("Burst", 0)
        val sampling = sharedPref.getBoolean("SalivaSampling", false)

        var text = String.format(getString(R.string.general_data), participantId, burst, totalSurveys)
        if (sampling)
            text += String.format(getString(R.string.general_sampling), totalProofs)

        if (sharedPref.getBoolean("ScheduleSurvey", false)) {
            val calendar = Calendar.getInstance()
            calendar.set(Calendar.HOUR_OF_DAY, 0)
            calendar.set(Calendar.MINUTE, 0)
            calendar.set(Calendar.SECOND, 0)
            calendar.set(Calendar.MILLISECOND, 0)
            calendar.add(Calendar.DATE, -2)
            val burstStartDate = calendar.time
            calendar.add(Calendar.DATE, 6)
            val burstEndDate = calendar.time
            val burstSurveys = totalSurveys - sharedPref.getInt("StartSurvey", 0) + 1
            val burstProofs = totalProofs - sharedPref.getInt("FirstProof", 0) + 1
            text += String.format(getString(R.string.burst_data), burst + 1, burstStartDate.toString(), burstEndDate.toString(), burstSurveys)
            if (sampling and arrayListOf(0, 2, 4).contains(burst))
                text += String.format(getString(R.string.burst_sampling), burstProofs)
        }

        total_data.text = text
    }

    override fun onResume() {
        super.onResume()
        updateStats()
    }
}
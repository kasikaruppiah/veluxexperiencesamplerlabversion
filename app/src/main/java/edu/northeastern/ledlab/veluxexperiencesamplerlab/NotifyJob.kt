package edu.northeastern.ledlab.veluxexperiencesamplerlab

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.net.Uri
import android.preference.PreferenceManager
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import com.evernote.android.job.Job
import com.evernote.android.job.JobRequest
import com.evernote.android.job.util.support.PersistableBundleCompat
import com.google.gson.Gson
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/**
 * Created by ledlab on 9/2/17.
 */
class NotifyJob : Job() {
    override fun onRunJob(params: Job.Params?): Job.Result {
        val extras = params!!.extras
        try {
            val text = extras.getString("text", context.getString(R.string.default_notification))
            val mBuilder = NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setContentTitle(context.getString(R.string.app_name))
                    .setContentText(text)
                    .setStyle(NotificationCompat.BigTextStyle().bigText(text))
                    .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                    .setAutoCancel(true)

            val soundURI = Uri.parse("android.resource://edu.northeastern.ledlab.veluxexperiencesamplerlab/" + R.raw.notification)
            mBuilder.setSound(soundURI)
            mBuilder.setVibrate(longArrayOf(1000, 1000))

            val resumeIntent = Intent(context, MainActivity::class.java)
            val stackBuilder = android.support.v4.app.TaskStackBuilder.create(context)
            stackBuilder.addParentStack(MainActivity::class.java)
            stackBuilder.addNextIntent(resumeIntent)
            val resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
            mBuilder.setContentIntent(resultPendingIntent)

            val mNotificationId = System.currentTimeMillis().toInt()
            val mNotifyMgr = context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            mNotifyMgr.notify(mNotificationId, mBuilder.build())

            return Job.Result.SUCCESS
        } finally {

        }
    }

    companion object {
        const val TAG = "job_notify_db"
        private val random = Random()

        private fun scheduleJob(day: Int, time: Int, text: String) {
            val hour = time / 60
            val minute = time % 60
            val executionWindow = ExecutionWindow(day, hour, minute)

            val extras = PersistableBundleCompat()
            extras.putString("text", text)

            JobRequest.Builder(NotifyJob.TAG).setExact(executionWindow.timeMs).setExtras(extras).setPersisted(true).build().schedule()
        }

        fun scheduleJobs(context: Context) {
            val today = Calendar.getInstance().time
            val newCal = Calendar.getInstance()
            newCal.time = today
            newCal.set(Calendar.HOUR_OF_DAY, 0)
            newCal.set(Calendar.MINUTE, 0)
            newCal.set(Calendar.SECOND, 0)
            newCal.set(Calendar.MILLISECOND, 0)
            var startDate = today
            var endDate = today
            val timeMap = HashMap<String, DayMap>()
            val simpleDateFormat = SimpleDateFormat("dd/M/yyyy")

            val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
            val weekdayWake = sharedPref.getInt(context.getString(R.string.weekday_wake_key), 360)
            val weekdaySleep = weekdayWake + 720
            val weekendWake = sharedPref.getInt(context.getString(R.string.weekend_wake_key), 480)
            val weekendSleep = weekendWake + 720
            val sampling = sharedPref.getBoolean("SalivaSampling", false)

            for (i in 1..5) {
                newCal.add(Calendar.DATE, 1)
                val date = newCal.time
                if (i == 1)
                    startDate = date
                timeMap[simpleDateFormat.format(date)] = scheduleNotificationForDay(context, newCal, weekdayWake, weekdaySleep, weekendWake, weekendSleep)
                if (sampling and (i == 1))
                    scheduleJob(newCal.get(Calendar.DAY_OF_WEEK), 1080, context.getString(R.string.remainder_a))
                PenalityJob.scheduleJob(newCal.timeInMillis)
                if (i == 5) {
                    newCal.add(Calendar.DATE, 1)
                    endDate = newCal.time
                    LastDayJob.scheduleJob(newCal.timeInMillis)
                }
            }

            val burstTimeMap = TimeMap(timeMap, startDate, endDate)

            DBUtils.addBurst(context, burstTimeMap)
        }

        private fun rand(from: Int, to: Int): Int {
            return random.nextInt(to - from) + from
        }

        private fun getSurveyTime(startTime: Int, endTime: Int): ArrayList<Int> {
            val surveyTimes = ArrayList<Int>()
            val interval = (endTime - startTime) / 6
            for (i in 1..6) {
                val surveyStartTime = startTime + interval * (i - 1)
                val surveyEndTime = startTime + interval * i - 40
                val surveyTime = rand(surveyStartTime, surveyEndTime)
                surveyTimes.add(surveyTime)
            }
            return surveyTimes
        }

        fun scheduleNotificationForDay(context: Context, calendar: Calendar, weekdayWake: Int, weekdaySleep: Int, weekendWake: Int, weekendSleep: Int): DayMap {
            val dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)
            val surveyTimes = when (dayOfWeek) {
                in arrayOf(Calendar.SATURDAY, Calendar.SUNDAY) -> getSurveyTime(weekendWake, weekendSleep)
                else -> getSurveyTime(weekdayWake, weekdaySleep)
            }
            var i = 0
            for (surveyTime in surveyTimes) {
                i++
                scheduleJob(dayOfWeek, surveyTime, String.format(context.getString(R.string.notification), i, 6))
            }
            return DayMap(surveyTimes)
        }

        fun scheduleSamplingNotifications(context: Context, date: Date) {
            val wakeUpCalendar = Calendar.getInstance()
            wakeUpCalendar.time = date
            wakeUpCalendar.add(Calendar.MINUTE, 30)
            scheduleSamplingJob(wakeUpCalendar.timeInMillis, context.getString(R.string.remainder_b))
            wakeUpCalendar.add(Calendar.MINUTE, 135)
            scheduleSamplingJob(wakeUpCalendar.timeInMillis, context.getString(R.string.remainder_c))
            wakeUpCalendar.add(Calendar.MINUTE, 600)
            val simpleDateFormat = SimpleDateFormat("dd/M/yyyy")
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
            val timeMap = Gson().fromJson(sharedPref.getString(context.getString(R.string.time_map_key), ""), TimeMap::class.java)
            val firstDateCalendar = Calendar.getInstance()
            firstDateCalendar.time = timeMap.startDate
            firstDateCalendar.add(Calendar.DATE, 1)
            if (simpleDateFormat.format(wakeUpCalendar.time) == simpleDateFormat.format(firstDateCalendar.time))
                scheduleSamplingJob(wakeUpCalendar.timeInMillis, context.getString(R.string.remainder_d))
            else
                scheduleSamplingJob(wakeUpCalendar.timeInMillis, context.getString(R.string.remainder_c))
        }

        private fun scheduleSamplingJob(time: Long, text: String) {
            val currentTime = Calendar.getInstance().timeInMillis
            var timeMs = time - currentTime
            if (timeMs < 0)
                timeMs = 1

            val extras = PersistableBundleCompat()
            extras.putString("text", text)

            JobRequest.Builder(NotifyJob.TAG).setExact(timeMs).setExtras(extras).setPersisted(true)
                    .build().schedule()
        }
    }
}
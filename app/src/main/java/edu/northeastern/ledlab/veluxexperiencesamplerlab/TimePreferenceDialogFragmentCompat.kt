package edu.northeastern.ledlab.veluxexperiencesamplerlab

import android.content.res.Resources
import android.os.Build
import android.os.Bundle
import android.support.v7.preference.PreferenceDialogFragmentCompat
import android.view.View
import android.widget.TimePicker

class TimePreferenceDialogFragmentCompat : PreferenceDialogFragmentCompat() {

    private var mTimePicker: TimePicker? = null

    override fun onBindDialogView(view: View) {
        super.onBindDialogView(view)

        mTimePicker = view.findViewById<View>(R.id.edit) as TimePicker

        if (mTimePicker == null) {
            throw IllegalStateException("Dialog view must contain a TimePicker with id 'edit'")
        }

        var minutesAfterMidnight: Int? = null
        val preference = preference
        if (preference is TimePreference) {
            minutesAfterMidnight = preference.time
        }

        if (minutesAfterMidnight != null) {
            val hours = minutesAfterMidnight / 60
            val minutes = minutesAfterMidnight % 60

            mTimePicker!!.setIs24HourView(false)
            mTimePicker!!.currentHour = hours
            mTimePicker!!.currentMinute = minutes
        }
        hideAmPmLayout(mTimePicker!!)

    }

    private fun hideAmPmLayout(picker: TimePicker) {
        val id = Resources.getSystem().getIdentifier("ampm_layout", "id", "android")
        val amPmLayout = picker.findViewById<View>(id)
        if (amPmLayout != null) {
            amPmLayout.visibility = View.GONE
        }
    }

    override fun onDialogClosed(positiveResult: Boolean) {
        if (positiveResult) {
            val hours: Int
            val minutes: Int
            if (Build.VERSION.SDK_INT >= 23) {
                hours = mTimePicker!!.hour
                minutes = mTimePicker!!.minute
            } else {
                hours = mTimePicker!!.currentHour
                minutes = mTimePicker!!.currentMinute
            }

            val minutesAfterMidnight = hours * 60 + minutes

            val dialogPreference = preference
            (dialogPreference as? TimePreference)?.callChangeListener(minutesAfterMidnight)
        }
    }

    companion object {

        fun newInstance(key: String): TimePreferenceDialogFragmentCompat {
            val fragment = TimePreferenceDialogFragmentCompat()
            val b = Bundle(1)
            b.putString(PreferenceDialogFragmentCompat.ARG_KEY, key)
            fragment.arguments = b

            return fragment
        }
    }
}
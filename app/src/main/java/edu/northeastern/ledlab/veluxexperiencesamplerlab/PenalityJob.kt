package edu.northeastern.ledlab.veluxexperiencesamplerlab

import com.evernote.android.job.Job
import com.evernote.android.job.JobRequest
import com.evernote.android.job.util.support.PersistableBundleCompat
import java.util.*

class PenalityJob : Job() {
    override fun onRunJob(params: Params?): Result {
        val extras = params!!.extras
        DBUtils.schedulePenalityJob(context, extras.getLong("timeInMS", Calendar.getInstance().timeInMillis))
        return Result.SUCCESS
    }

    companion object {
        const val TAG = "job_penality_db"
        fun scheduleJob(timeInMS: Long) {
            val currentTime = Calendar.getInstance().timeInMillis
            var timeMs = timeInMS - currentTime + 86400000
            if (timeMs < 0)
                timeMs = 1
            val extras = PersistableBundleCompat()
            extras.putLong("timeInMS", timeInMS)
            JobRequest.Builder(PenalityJob.TAG).setExact(timeMs).setExtras(extras).setPersisted(true).build().schedule()
        }
    }
}
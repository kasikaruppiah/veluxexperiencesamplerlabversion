package edu.northeastern.ledlab.veluxexperiencesamplerlab

import android.Manifest
import android.app.Activity
import android.app.TimePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.preference.PreferenceManager
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.widget.Toast
import com.bumptech.glide.Glide
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_hormone_sampling.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class HormoneSamplingActivity : BaseActivity() {
    private val REQUEST_IMAGE_CAPTURE = 2
    private val REQUEST_EXTERNAL_STORAGE = 3

    private var mStorageRef: StorageReference? = null
    private var mProofPhotosStorageReference: StorageReference? = null
    private var pid: String? = null
    private var imageFileLocation: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hormone_sampling)

        mStorageRef = FirebaseStorage.getInstance().reference
        mProofPhotosStorageReference = mStorageRef!!.child("HormoneSamplingProof")
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        pid = sharedPref.getString(this.getString(R.string.pid), "unknown")

        val simpleDateFormat = SimpleDateFormat("dd/M/yyyy")

        val wakeUpCalendar = Calendar.getInstance()

        val samplingCalendar = Calendar.getInstance()
        val currentDate = simpleDateFormat.format(samplingCalendar.time)

        val samplingHour = samplingCalendar.get(Calendar.HOUR_OF_DAY)
        val samplingMinute = samplingCalendar.get(Calendar.MINUTE)
        var scheduleNotification = false

        if (intent.getSerializableExtra("WakeUpTime") != null) {
            wakeUpCalendar.time = intent.getSerializableExtra("WakeUpTime") as Date

            val wakeUpHour = wakeUpCalendar.get(Calendar.HOUR_OF_DAY)
            val wakeUpMinute = wakeUpCalendar.get(Calendar.MINUTE)
            wakeup_time.text = "${String.format("%02d", wakeUpHour)}:${String.format("%02d", wakeUpMinute)}"
            wakeup_time.isEnabled = false
        } else {
            wakeup_time.text = "${String.format("%02d", samplingHour)}:${String.format("%02d", samplingMinute)}"
            scheduleNotification = true
        }

        wakeup_time.setOnClickListener {
            val time = wakeup_time.text.split(":")
            val timePickerDialog = TimePickerDialog(this, R.style.AppAlertDialog,
                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                        wakeUpCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                        wakeUpCalendar.set(Calendar.MINUTE, minute)
                        wakeup_time.text = "${String.format("%02d", hourOfDay)}:${String.format("%02d", minute)}"
                    },
                    time[0].toInt(), time[1].toInt(), false)
            timePickerDialog.show()
        }

        sampling_time.text = "${String.format("%02d", samplingHour)}:${String.format("%02d", samplingMinute)}"
        sampling_time.setOnClickListener {
            val time = sampling_time.text.split(":")
            val timePickerDialog = TimePickerDialog(this, R.style.AppAlertDialog,
                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                        samplingCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                        samplingCalendar.set(Calendar.MINUTE, minute)
                        sampling_time.text = "${String.format("%02d", hourOfDay)}:${String.format("%02d", minute)}"
                    },
                    time[0].toInt(), time[1].toInt(), false)
            timePickerDialog.show()
        }

        image_view.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission
                                .WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                    takePhoto()
                else {
                    if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE))
                        Toast.makeText(this, getString(R.string.writePermission), Toast.LENGTH_SHORT).show()
                    requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_EXTERNAL_STORAGE)
                }
            } else
                takePhoto()
        }

        upld_image.isEnabled = false
        upld_image.setOnClickListener {
            finish()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_EXTERNAL_STORAGE)
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                takePhoto()
            else
                Toast.makeText(this, getString(R.string.writeDenied), Toast.LENGTH_SHORT).show()
        else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            Glide.with(this).load(imageFileLocation).into(image_view)
            upld_image.isEnabled = true
        }
    }

    private fun takePhoto() {
        val cameraIntent = Intent()
        cameraIntent.action = MediaStore.ACTION_IMAGE_CAPTURE
        if (cameraIntent.resolveActivity(packageManager) != null) {
            var image: File? = null
            try {
                image = createImageFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            if (image != null) {
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(this,
                        BuildConfig.APPLICATION_ID + ".provider", image))
                startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    private fun createImageFile(): File {
        val timestamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val fileName = "${timestamp}_"
        val storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(fileName, ".jpg", storageDirectory)
        imageFileLocation = image.absolutePath

        return image
    }
}

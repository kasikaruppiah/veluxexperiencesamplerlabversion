package edu.northeastern.ledlab.veluxexperiencesamplerlab

import android.os.Bundle

/**
 * Created by ledlab on 8/15/17.
 */
class SettingsActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_settings)
    }
}
package edu.northeastern.ledlab.veluxexperiencesamplerlab

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.text.Editable
import android.text.TextWatcher
import kotlinx.android.synthetic.main.activity_goal.*

class GoalActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_goal)

        goal1.setText(sharedPref.getString("Goal1", ""))
        goal2.setText(sharedPref.getString("Goal2", ""))
        goal3.setText(sharedPref.getString("Goal3", ""))

        goal1.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                //TODO("not implemented") //To change body of created functions use File |
                // Settings | File Templates.
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                //TODO("not implemented") //To change body of created functions use File |
                // Settings | File Templates.
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                update_goals.isEnabled = enableUpdateButton()
            }
        })

        goal2.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                //TODO("not implemented") //To change body of created functions use File |
                // Settings | File Templates.
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                //TODO("not implemented") //To change body of created functions use File |
                // Settings | File Templates.
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                update_goals.isEnabled = enableUpdateButton()
            }
        })

        goal3.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                //TODO("not implemented") //To change body of created functions use File |
                // Settings | File Templates.
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                //TODO("not implemented") //To change body of created functions use File |
                // Settings | File Templates.
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                update_goals.isEnabled = enableUpdateButton()
            }
        })

        update_goals.isEnabled = enableUpdateButton()
        update_goals.setOnClickListener {
            val prefEditor = sharedPref.edit()
            prefEditor.putString("Goal1", goal1.text.toString())
            prefEditor.putString("Goal2", goal2.text.toString())
            prefEditor.putString("Goal3", goal3.text.toString())
            prefEditor.apply()
            val intent = Intent(this, LoadingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            intent.putExtra("scheduleSurvey", true)
            startActivity(intent)
        }
    }

    private fun enableUpdateButton(): Boolean {
        return !(goal1.text.isNullOrEmpty() || goal2.text.isNullOrEmpty() || goal3.text.isNullOrEmpty())
    }
}

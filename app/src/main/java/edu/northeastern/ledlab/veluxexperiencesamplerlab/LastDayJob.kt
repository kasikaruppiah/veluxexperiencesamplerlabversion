package edu.northeastern.ledlab.veluxexperiencesamplerlab

import com.evernote.android.job.Job
import com.evernote.android.job.JobRequest
import java.util.*

class LastDayJob : Job() {
    override fun onRunJob(params: Params?): Result {
        DBUtils.updateBurstCompletion(context)
        return Result.SUCCESS
    }

    companion object {
        const val TAG = "job_last_day_db"
        fun scheduleJob(targetTime: Long) {
            val currentTime = Calendar.getInstance().timeInMillis
            var timeMs = targetTime - currentTime + 3600000
            if (timeMs < 0)
                timeMs = 1
            JobRequest.Builder(LastDayJob.TAG).setExact(timeMs).setPersisted(true).build().schedule()
        }
    }
}
package edu.northeastern.ledlab.veluxexperiencesamplerlab

import android.os.AsyncTask
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_loading.*


/**
 * Created by ledlab on 8/23/17.
 */
class LoadingActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading)
        when {
            intent.getBooleanExtra("registerUser", false) -> {
                loading_text.text = getString(R.string.loading_reg_txt)
                DBUtils.registerUser(this)
            }
            intent.getBooleanExtra("scheduleSurvey", false) -> {
                loading_text.text = getString(R.string.updating_user_goals)
                DBUtils.retrieveUserGoals(this)
            }
            intent.getBooleanExtra("addNotification", false) -> {
                loading_text.text = getString(R.string.updt_notification)
                AddNotification().execute()
            }
            intent.getBooleanExtra("syncData", false) -> {
                loading_text.text = getString(R.string.loading_upld_txt)
                val pid = intent.getStringExtra("pid")
                DBUtils.syncData(this)
            }
        }
    }

    private inner class AddNotification : AsyncTask<Unit, Unit, Unit>() {
        override fun doInBackground(vararg params: Unit?) {
            NotifyJob.scheduleJobs(this@LoadingActivity)
        }
    }
}
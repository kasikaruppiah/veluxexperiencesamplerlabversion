package edu.northeastern.ledlab.veluxexperiencesamplerlab

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

/**
 * Created by ledlab on 8/15/17.
 */
class MenuFragmentPageAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return if (position == 0)
            SurveyFragment()
        else
            UploadFragment()
    }

    override fun getCount(): Int = 2
}
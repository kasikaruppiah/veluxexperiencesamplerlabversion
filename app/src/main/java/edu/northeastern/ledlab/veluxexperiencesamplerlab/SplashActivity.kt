package edu.northeastern.ledlab.veluxexperiencesamplerlab

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager

/**
 * Created by ledlab on 8/24/17.
 */
class SplashActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        val pReg = sharedPref.getBoolean(getString(R.string.preg), false)

        var intent = Intent(this, LoginActivity::class.java)
        if (pReg) {
            intent = Intent(this, MainActivity::class.java)
        }
        startActivity(intent)
        finish()
    }
}
package edu.northeastern.ledlab.veluxexperiencesamplerlab

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity


abstract class BaseActivity : AppCompatActivity() {
    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase!!))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        resetTitles()
    }

    private fun resetTitles() {
        supportActionBar?.title = getString(R.string.app_name)
    }
}
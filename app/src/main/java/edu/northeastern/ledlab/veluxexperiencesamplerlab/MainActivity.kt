package edu.northeastern.ledlab.veluxexperiencesamplerlab

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    private val imageResId = listOf(R.drawable.ic_survey, R.drawable.ic_upload)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        view_pager.adapter = MenuFragmentPageAdapter(supportFragmentManager)
        tab_layout.setupWithViewPager(view_pager)

        for (i in 0 until tab_layout.tabCount)
            tab_layout.getTabAt(i)!!.setIcon(imageResId[i])
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_settings, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == R.id.settings) {
            val settingsIntent = Intent(this, SettingsActivity::class.java)
            startActivity(settingsIntent)
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}

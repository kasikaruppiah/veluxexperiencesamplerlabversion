package edu.northeastern.ledlab.veluxexperiencesamplerlab

import android.util.Log
import com.evernote.android.job.Job
import com.evernote.android.job.JobRequest
import java.util.concurrent.TimeUnit


/**
 * Created by ledlab on 9/1/17.
 */
class SyncJob : Job() {
    override fun onRunJob(params: Params?): Result {
        Log.d("KAKA", "inside::SyncJob::onRunJob")
        DBUtils.syncData(context)

        return Result.SUCCESS
    }

    companion object {
        const val TAG = "job_sync_db"
        fun scheduleJob() {
            Log.d("KAKA", "inside::SyncJob::scheduleJob")
            JobRequest.Builder(SyncJob.TAG).setPeriodic(TimeUnit.HOURS.toMillis(3), TimeUnit.MINUTES.toMillis(30)).setRequiredNetworkType(JobRequest.NetworkType.CONNECTED).setRequirementsEnforced(true).setUpdateCurrent(true).setPersisted(true).build().schedule()
        }
    }
}